'use strict';

var goodsInOrder = 0,
    goodsInStock = 0;

if (goodsInOrder > goodsInStock) {
  console.log('На складе нет такого количества товаров');
}
else if (goodsInOrder === goodsInStock) {
  console.log('Вы забираете весь товар c нашего склада!');
}
else {
  console.log('Заказ оформлен');
}


var place = '',
    priceForMoon = '150',
    priceForCrabNebula = '250',
    priceForAndromedaGalaxy = '550',
    priceForNeonNebula = '600',
    priceForTheDeathStar = 'Договорная цена';

switch(place) {
  case 'Луна':
    console.log(`Стоимость доставки для области ${place}: ${priceForMoon} Q`);
    break;
  case 'Крабовидная туманность':
    console.log(`Стоимость доставки для области ${place}: ${priceForCrabNebula} Q`);
    break;
  case 'Галактика Туманность Андромеды':
    console.log(`Стоимость доставки для области ${place}: ${priceForAndromedaGalaxy} Q`);
    break;
  case 'Туманность Ориона':
    console.log(`Стоимость доставки для области ${place}: ${priceForNeonNebula} Q`);
    break;
  case 'Звезда смерти':
    console.log(`Стоимость доставки для области ${place}: ${priceForTheDeathStar}`);
    break;
  default:
    console.log(`В ваш квадрант доставка не осуществляется`);
}


var enteredPrice = 0;

try {
  if (typeof(enteredPrice) != `number`) {
    throw `Вы допустили ошибку: ${enteredPrice} не является числом`;
  }
  console.log(`Цена товара введена корректно`);
}
catch(err) {
  console.log(err);
}


var planet = '',
    age = 0;

if (planet === 'Земля' && age >= 18) {
  console.log(`Приятных покупок`);
}
else if (planet === 'Земля' && age < 18) {
  console.log(`Вы не достигли совершеннолетия`);
}
else if (planet === 'Юпитер' && age >= 120) {
  console.log(`Чистого неба и удачных покупок!`);
}
else if (planet === 'Юпитер' && age < 120) {
  console.log(`Вернитесь на 120-й день рождения!`);
}
else {
  console.log(`Спасибо, что пользуетесь услугами нашего магазина!`);
}